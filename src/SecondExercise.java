import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class SecondExercise {
    public static void main(String[] args){
        new Window();
    }
}

class Window extends JFrame implements ActionListener {
    JButton press;
    JTextField fileName, inputText;
    Window(){
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setTitle("Find in text");
        setBounds(300, 100, 400, 500);

        press = new JButton("send");
        press.setBounds(100,50,100,30);
        press.addActionListener(this);
        add(press);

        fileName = new JTextField();
        fileName.setBounds(0,10,300,30);
        add(fileName);

        inputText = new JTextField();
        inputText.setBounds(0,100,300,250);
        add(inputText);

        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(press)){
            try {
                FileWriter fileWriter = new FileWriter(fileName.getText());
                fileWriter.write(inputText.getText());
                fileWriter.flush();
                fileWriter.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }
}
