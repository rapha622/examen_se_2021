import java.util.ArrayList;

public class Car extends Vehicle{
    private String color;
    Wheel frontLeft, frontRight, backLeft, backRight;
    WindShield windShield;
    Car(String name, String color){
        super(name);
        this.color = color;
        frontLeft = new Wheel();
        frontRight = new Wheel();
        backLeft = new Wheel();
        backRight = new Wheel();
        windShield = new WindShield();
    }

    public void start(){
        System.out.println("Car starts");
    }
    public void stop(){
        System.out.println("Car stops");
    }
    public void go(){
        System.out.println("Car goes");
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}

class Vehicle{
    String name;
    Vehicle(String name){
        this.name = name;
    }
}

class Wheel{
    Wheel(){
        pressure = 3;
    }
    int pressure;

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }
}
class CarMaker{
    ArrayList<Car> carsCreated = new ArrayList<>();
    public void addCar(Car newCar){
        carsCreated.add(newCar);
    }

    public ArrayList<Car> getCarsCreated() {
        return carsCreated;
    }
}

class User{
    String name;
    Car car;
    User(String name, Car car){
        this.name = name;
        this.car = car;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getName() {
        return name;
    }

    public Car getCar() {
        return car;
    }
}

class WindShield{
    int integrity;
    WindShield(){
        integrity = 100;
    }

    public int getIntegrity() {
        return integrity;
    }

    public void setIntegrity(int integrity) {
        this.integrity = integrity;
    }
}
